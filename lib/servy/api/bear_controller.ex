defmodule Servy.Api.BearController do
  alias Servy.Conv

  def index(%Conv{} = conv) do
    json =
      Servy.Wildthings.list_bears()
      |> Poison.encode!

    Conv.populate_response(conv, 200, json, "application/json")
  end

  def create(%Conv{} = conv) do
    Conv.populate_response(conv, 201, "Created a #{conv.params["type"]} bear named #{conv.params["name"]}!")
  end
end
