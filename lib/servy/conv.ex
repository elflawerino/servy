defmodule Servy.Conv do
  defstruct [
    method: "",
    path: "",
    params: %{},
    headers: %{},
    resp_headers: %{},
    resp_body: "",
    status: nil
  ]

  def populate_response(conv, status, body \\ nil, content_type \\ "text/html")
  def populate_response(%Servy.Conv{} = conv, status, body, content_type) when is_binary(body) do
    headers =
      conv.resp_headers
      |> Map.put("Content-Type", content_type)
      |> Map.put("Content-Length", String.length(body))

    %{conv | status: status, resp_body: body, resp_headers: headers}
  end

  def populate_response(%Servy.Conv{} = conv, status, _body, _content_type) do
    %{conv | status: status}
  end


  def full_status(%Servy.Conv{} = conv) do
    "#{conv.status} #{status_reason(conv.status)}"
  end

  @doc """
  Expands the response header map into a string for use in a HTTP response

  ## Example
    iex> conv = %Servy.Conv{resp_headers: %{"A" => "B", "C" => "D"}}
    iex> Servy.Conv.expand_headers(conv)
    "A: B\\r\\nC: D\\r\\n"
  """
  def expand_headers(%Servy.Conv{resp_headers: headers}) do
    headers
    |> Enum.sort
    |> Enum.reduce("", fn({key, value}, acc) ->
      acc <> "#{key}: #{value}\r\n"
    end)

  end

  defp status_reason(code) do
    %{
      200 => "OK",
      201 => "Created",
      401 => "Unauhorized",
      403 => "Forbidden",
      404 => "Not Found",
      500 => "Internal Server Error"
    }[code]
  end
end
