defmodule Servy.FileHandler do
  @pages_path Path.expand("pages", File.cwd!)
  @template_path Path.expand("templates", File.cwd!)

  require Earmark

  alias Servy.Conv

  def resolve_page_path(page) do
    Path.join(@pages_path, page)
  end

  def resolve_template_path(template) do
    Path.join(@template_path, template)
  end

  def handle_page(page, %Conv{} = conv) do
    resolve_page_path(page)
    |> File.read
    |> handle_file(page, conv)
  end

  def handle_file({:ok, content}, page, %Conv{} = conv) do
    {status, content} = case String.split(page, ".") do
      [_, "md"] -> {200, Earmark.as_html!(content)}
      [_, "html"] -> {200, content}
      [_, _] -> {500, "Unsupported page type"}
    end
    %{ conv | status: status, resp_body: content }
  end

  def handle_file({:error, :enoent}, _, %Conv{} = conv) do
    %{ conv | status: 404, resp_body: "File not found" }
  end

  def handle_file({:error, reason}, _, %Conv{} = conv) do
    %{ conv | status: 500, resp_body: "File error #{reason}" }
  end
end
