defmodule Servy.Parser do
  alias Servy.Conv # Results in an alias "Conv". Equivalent to "alias Servy.Conv, as: Conv"
  def parse(request) do
    [top, params] = String.split(request, "\r\n\r\n")
    [request | headers] = top |> String.split("\r\n")
    [method, path, _] = request |> String.split(" ")
    headers = parse_headers(headers)
    %Conv{
      method: method,
      path: path,
      headers: headers,
      params: parse_body(headers["Content-Type"], params)
    }
  end

  @doc """
  Parses parameters from a string to a map. Returns an empty map for unsupported content types.

  ## Examples
    iex> Servy.Parser.parse_body("application/x-www-form-urlencoded", "name=Baloo&type=Brown")
    %{"name" => "Baloo", "type" => "Brown"}
    iex> Servy.Parser.parse_body("application/json", "{\\"name\\": \\"Baloo\\", \\"type\\": \\"Brown\\"}")
    %{"name" => "Baloo", "type" => "Brown"}
    iex> Servy.Parser.parse_body("multipart/form-data", "name=Baloo&type=Brown")
    %{}
  """
  def parse_body("application/x-www-form-urlencoded", params_string) do
    params_string |> String.trim |> URI.decode_query
  end

  def parse_body("application/json", params_string) do
    Poison.decode!(params_string)
  end

  def parse_body(_,_), do: %{}

  @doc """
  Parses a list of HTTP-style headers into a map.

  ## Examples
    iex> Servy.Parser.parse_headers(["A: B", "C: D"])
    %{"A" => "B", "C" => "D"}
  """
  def parse_headers(headers) do
    Enum.reduce(headers, %{}, fn(header, map) ->
      [key, value] = String.split(header, ": ")
      Map.put(map, key, value)
    end)
  end
end
