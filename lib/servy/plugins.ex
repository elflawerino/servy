defmodule Servy.Plugins do
  require Logger
  alias Servy.Conv
  def emojify(%Conv{ status: 200, resp_body: original_body } = conv) do
    %{ conv | resp_body: ":) #{original_body} :)"}
  end

  def emojify(%Conv{} = conv), do: conv

  def rewrite_path(%Conv{ path: "/wildlife" } = conv) do
    %{conv | path: "/wildthings"}
  end

  def rewrite_path(%Conv{ path: path } = conv) do
    regex = ~r{\/(?<thing>\w+)\?id=(?<id>\d+)}
    captures = Regex.named_captures(regex, path)
    rewrite_path_captures(conv, captures)
  end

  def rewrite_path(%Conv{} = conv), do: conv

  def rewrite_path_captures(%Conv{} = conv, %{"thing" => thing, "id" => id}) do
    %{ conv | path: "/#{thing}/#{id}" }
  end

  def rewrite_path_captures(%Conv{} = conv, nil), do: conv

  def track(%Conv{status: 404, path: path} = conv) do
    Logger.debug("Warning: #{path} is on the loose!")
    conv
  end

  def track(%Conv{} = conv), do: conv

  def log(conv) do
    Logger.debug("#{inspect(conv)}")
    conv
  end
end
