defmodule Servy.View do
  import Servy.FileHandler, only: [resolve_template_path: 1]

  alias Servy.Conv

  def render(%Conv{} = conv, template, bindings \\ []) do
    content =
      resolve_template_path(template)
      |> EEx.eval_file(bindings)

    Conv.populate_response(conv, 200, content)
  end
end
