defmodule Servy.Wildthings do
  @db_path Path.expand("db", File.cwd!)

  alias Servy.Bear

  def list_bears do
    %{"bears" => bears} =
    Path.join(@db_path, "bears.json")
    |> File.read
    |> case do
      {:ok, content} -> Poison.decode!(content, as: %{"bears" => [%Bear{}]})
      {:error, _} -> []
    end

    bears
  end

  def get_bear(id) when is_integer(id) do
    Enum.find(list_bears(), fn(b) -> b.id == id end)
  end

  def get_bear(id) when is_binary(id) do
    id |> String.to_integer |> get_bear()
  end
end
