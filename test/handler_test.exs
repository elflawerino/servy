defmodule HandlerTest do
  use ExUnit.Case, async: true
  doctest Servy.Handler

  test "gets faq" do
    request = """
    GET /faq HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    %{"code" => code, "status" => status} =
      Servy.Handler.handle(request)
      |> parse_http_response

    assert status == "OK"
    assert code == "200"
  end

  test "gets bears" do
    request = """
    GET /bears HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    %{"code" => code, "status" => status} =
      Servy.Handler.handle(request)
      |> parse_http_response

    assert status == "OK"
    assert code == "200"
  end

  test "gets a bear" do
    request = """
    GET /bears/1 HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    %{"code" => code, "status" => status} =
      Servy.Handler.handle(request)
      |> parse_http_response

    assert status == "OK"
    assert code == "200"
  end

  test "makes a bear" do
    request = """
    POST /bears HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    Content-Type: application/x-www-form-urlencoded\r
    Content-Length: 21\r
    \r
    name=Baloo&type=Brown
    """

    %{"code" => code, "status" => status} =
      Servy.Handler.handle(request)
      |> parse_http_response

    assert status == "Created"
    assert code == "201"
  end

  test "deletes a bear" do
    request = """
    DELETE /bears/1 HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    %{"code" => code, "status" => status} =
      Servy.Handler.handle(request)
      |> parse_http_response

    assert status == "Forbidden"
    assert code == "403"
  end

  test "GET /api/bears" do
    request = """
    GET /api/bears HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    response = Servy.Handler.handle(request)

    expected_response = """
    HTTP/1.1 200 OK\r
    Content-Length: 605\r
    Content-Type: application/json\r
    \r
    [{"type":"Brown","name":"Teddy","id":1,"hibernating":true},
     {"type":"Black","name":"Smokey","id":2,"hibernating":false},
     {"type":"Brown","name":"Paddington","id":3,"hibernating":false},
     {"type":"Grizzly","name":"Scarface","id":4,"hibernating":true},
     {"type":"Polar","name":"Snow","id":5,"hibernating":false},
     {"type":"Grizzly","name":"Brutus","id":6,"hibernating":false},
     {"type":"Black","name":"Rosie","id":7,"hibernating":true},
     {"type":"Panda","name":"Roscoe","id":8,"hibernating":false},
     {"type":"Polar","name":"Iceman","id":9,"hibernating":true},
     {"type":"Grizzly","name":"Kenai","id":10,"hibernating":false}]
    """

    assert remove_whitespace(response) == remove_whitespace(expected_response)
  end

  test "POST /api/bears" do
    request = """
    POST /api/bears HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    Content-Type: application/json\r
    Content-Length: 21\r
    \r
    {"name": "Breezly", "type": "Polar"}
    """

    response = Servy.Handler.handle(request)

    assert response == """
    HTTP/1.1 201 Created\r
    Content-Length: 35\r
    Content-Type: text/html\r
    \r
    Created a Polar bear named Breezly!
    """
  end

  defp parse_http_response(response) do
    first_line =
      response
      |> String.split("\r\n")
      |> List.first

    Regex.named_captures(~r{(?<version>.+) (?<code>.+) (?<status>.+)}, first_line)
  end

  defp remove_whitespace(text) do
    String.replace(text, ~r{\s}, "")
  end
end
